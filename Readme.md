# Script for stitching 360 images from Insta 360 One X on linux, with a basic web gallery.

Using Hugin - http://hugin.sourceforge.net/ - opensource panorama photo stitcher

With instructions inspired by: https://invidio.us/watch?v=QKQGT8VUN8g

The important parameters for Insta 360 One X are:
- lens type: circular fisheye
- image has to be loaded twice, set one as lense 0, other as lense 1
- lens HFOV (in custom optimizer params) is 397 (it changes the focal length too to 5.585?)
- set one lense to roll -90, "d" -1520, the other lense yaw 180, roll 90, "d" 1520
- crop must be applied to both images (t,l,r,b) - 0,0,3040,3040 and 0,3040,6080,3040
- then search for control points using Hugin's CPFind
- then apply Geometric Optimization - only for position
- then go to preview, move/drag, adjust position as needed (this is how you can also set the "center" - what will be in view first time the image is loaded)
- finally stitch as equirectangular jpg, make sure the vertical FOV is 180

# How to use

1. Install Hugin: `sudo apt-get install hugin`
2. Have your original photos from Insta360 One X in one directory, let's call it `./originals`
3. Run the stitching script: `./process.sh -o ./output ./originals/*`
4. The script will open Hugin at one point, allowing you to adjust the orientation of the camera: open the Fast Preview, go to Move/Drag tab, adjust position, save project, exit
5. When all images are processed, use the thumbs script to generate thumbnails for the web gallery: `./thumbs.sh -o ./output/thumbs ./output/*.jpg`
6. Copy .env.dist file into .env and Edit the path to the gallery (./output)
7. Run the docker container with web gallery: `docker-compose up`
8. Open your browser at http://localhost:3080/

The web interface is very basic, only serves as proof of concept.
