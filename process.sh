#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILE=
OUTPUTDIR=
SKIPOPT=0
NEEDSCLEANUP=0

while getopts "o:np:" OPT; do
    case "$OPT" in
        o)
            OUTPUTDIR=$OPTARG
            ;;
        n)
            SKIPOPT=1
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Missing option argument for -$OPTARG" >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND -1)) # skip to the remaining argument after options
if [ "" = "$1" ]; then
    echo "Empty file argument" >&2
    exit 1
fi

for FILE in "$@"
do
    FILE=`readlink -f "$FILE"`
    echo "Using file: $FILE"

    if [ ! -f "$FILE" ]; then
        echo "Specified file does not exist or is not a file" >&2
        continue
    fi
    echo "Processing $FILE"

    FILENAME="${FILE##*/}"
    FILEPATH="${FILE%/$FILENAME}"
    EXT="${FILENAME#*.}"
    FILENAMENOEXT="${FILENAME%.$EXT}"

    # echo $FILENAME
    # echo $FILEPATH
    # echo $EXT
    # echo $FILENAMENOEXT

    echo "parsed path: $FILEPATH / $FILENAMENOEXT . $EXT"
    if [ "" = "$OUTPUTDIR" ]; then
        OUTPUTDIR=$FILEPATH
    fi
    OUTPUTDIR=`readlink -f "$OUTPUTDIR"`
    echo "Using output dir $OUTPUTDIR"

    # determine the actual used tmp dir
    # find if there are any previous dangling temp dirs
    TESTTMPDIR=`mktemp -d -u -t "testing-XXXXXXXX"`
    TESTTMPFILEPATH="${TESTTMPDIR%/testing-*}"
    echo
    ls -1d $TESTTMPFILEPATH/tmp.hugin-360-process.* 2>/dev/null # must be unquoted for the asterisk to work
    CODE=$?
    # echo $CODE
    if [ $CODE -eq 0 ]; then
        echo "Some previous temp dirs detected"
        echo "Use 'rm -fr $TESTTMPFILEPATH/tmp.hugin-360-process.*' to clean them"
        echo
    fi

    TMPDIR=`mktemp -d -p "$TESTTMPFILEPATH" "tmp.hugin-360-process.XXXXXXX"`
    NEEDSCLEANUP=1

    echo "Using temp dir $TMPDIR"

    echo "Copying input image to $TMPDIR/$FILENAME"

    cp "$FILE" "$TMPDIR/$FILENAME"

    PROJECTFILE="$TMPDIR/$FILENAMENOEXT.pto"

    # generate project file for this image from template
    sed "s#<FILENAME>#$TMPDIR/$FILENAME#" $DIR/template.pto > $PROJECTFILE
    # find control points with cpfind, optimize with celeste
    cpfind -o $PROJECTFILE --multirow --celeste $PROJECTFILE
    # clean control points - remove blue sky or control points too far
    cpclean -o $PROJECTFILE $PROJECTFILE

    # FIXME: if only we could somehow get the gyroscope data from insta360 for each image
    if [ "1" = "$SKIPOPT" ]; then
        echo "Optimization skipped"
    else
        # find vertical lines
        linefind -o $PROJECTFILE $PROJECTFILE
        # set to optimize the position parameters only (yaw, pitch, roll)
        # pto_var --opt y,p,r -o $PROJECTFILE $PROJECTFILE
        # optimize by pre-set parameters from pto_var
        # autooptimiser -n -o $PROJECTFILE $PROJECTFILE

        # optimize alignment (-a)
        # pairwise optimization of yaw, pitch, roll (-p)
        # level horizon (-l) doesn't work so well for insta360 because of the lens orientation
        # set suitable projection and size (-s)
        # exposure optimization (-m) sometimes makes problems
        autooptimiser -a -p -s -o $PROJECTFILE $PROJECTFILE
        # autooptimiser -m -o $PROJECTFILE $PROJECTFILE
    fi

    # best fit crop the final image
    # full size needs to be 4096x2048 to satisfy three.js
    # or we could do convert -resize 4096x2048 "$TMPDIR/$FILENAMENOEXT-eq.jpg" "$TMPDIR/$FILENAMENOEXT-eq.jpg"
    # pano_modify --canvas=AUTO --crop=AUTO -o $PROJECTFILE $PROJECTFILE
    pano_modify --canvas=4096x2048 --crop=AUTO -o $PROJECTFILE $PROJECTFILE

    # allow manual adjustment
    # open the Fast Preview, go to Move/Drag tab, adjust position, save project, exit
    hugin $PROJECTFILE

    # run the stitching now
    hugin_executor --stitching --prefix="$TMPDIR/$FILENAMENOEXT-eq" $PROJECTFILE # this still creates some temporary tiff files in the prefix dir

    mkdir -p "$OUTPUTDIR"
    cp "$TMPDIR/$FILENAMENOEXT-eq.jpg" "$OUTPUTDIR/"
    # cp $PROJECTFILE "$OUTPUTDIR/" # no, project file keeps relative references to the input images, this breaks them
    cp $PROJECTFILE "$FILEPATH/"

    # cleanup
    if [ "1" = "$NEEDSCLEANUP" ]; then
        rm -fr "$TMPDIR"
    fi

    echo
    echo "####################################################################"
    echo
done
