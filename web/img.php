<?php
$file = 'gallery/' . preg_replace('~[^a-zA-Z0-9_\.-]*~', '', $_GET['i']);
$img = '../' . $file;
if (!file_exists(__DIR__ . '/' . $img)) {
	die("file does not exist");
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>360</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
		<link type="text/css" rel="stylesheet" href="main.css">
		<script src="three.min.js"></script>
	</head>
	<body>

		<div id="container"></div>

		<script type="module">

			/**
			 * Shows a 2D loading indicator while various pieces of EmbedVR load.
			 */
			function LoadingIndicator() {
				this.el = this.build_();
				document.body.appendChild(this.el);
				this.show();
			}

			LoadingIndicator.prototype.build_ = function() {
				var overlay = document.createElement('div');
				var s = overlay.style;
				s.position = 'fixed';
				s.top = 0;
				s.left = 0;
				s.width = '100%';
				s.height = '100%';
				s.background = '#eee';
				var img = document.createElement('img');
				img.src = 'loading.gif';
				var s = img.style;
				s.position = 'absolute';
				s.top = '50%';
				s.left = '50%';
				s.transform = 'translate(-50%, -50%)';

				overlay.appendChild(img);
				return overlay;
			};

			LoadingIndicator.prototype.hide = function() {
				this.el.style.display = 'none';
			};

			LoadingIndicator.prototype.show = function() {
				this.el.style.display = 'block';
			};

			var camera, scene, renderer;

			var isUserInteracting = false,
				onMouseDownMouseX = 0, onMouseDownMouseY = 0,
				lon = 0, onMouseDownLon = 0,
				lat = 0, onMouseDownLat = 0,
				phi = 0, theta = 0;

			// start spinner
			var loadIndicator = new LoadingIndicator();
			init();

			function init() {
				var container, mesh;

				container = document.getElementById( 'container' );

				camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1100 );
				camera.target = new THREE.Vector3( 0, 0, 0 );

				scene = new THREE.Scene();

				var geometry = new THREE.SphereBufferGeometry( 500, 60, 40 );
				// invert the geometry on the x-axis so that all of the faces point inward
				geometry.scale( - 1, 1, 1 );

				var texture = new THREE.TextureLoader().load(
					'<?php echo htmlspecialchars($img); ?>',
					() => {
						// stop spinner
						loadIndicator.hide();
						requestAnimationFrame(update)
					}
				);
				var material = new THREE.MeshBasicMaterial( { map: texture } );

				mesh = new THREE.Mesh( geometry, material );

				scene.add( mesh );

				renderer = new THREE.WebGLRenderer();
				renderer.setPixelRatio( window.devicePixelRatio );
				renderer.setSize( window.innerWidth, window.innerHeight );
				container.appendChild( renderer.domElement );

				document.addEventListener( 'mousedown', onPointerStart, false );
				document.addEventListener( 'mousemove', onPointerMove, false );
				document.addEventListener( 'mouseup', onPointerUp, false );

				document.addEventListener( 'wheel', onDocumentMouseWheel, false );

				document.addEventListener( 'touchstart', onPointerStart, false );
				document.addEventListener( 'touchmove', onPointerMove, false );
				document.addEventListener( 'touchend', onPointerUp, false );

				window.addEventListener( 'resize', onWindowResize, false );
			}

			function onWindowResize() {
				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();

				renderer.setSize( window.innerWidth, window.innerHeight );
				requestAnimationFrame(update);
			}

			function onPointerStart( event ) {
				isUserInteracting = true;

				var clientX = event.clientX || event.touches[ 0 ].clientX;
				var clientY = event.clientY || event.touches[ 0 ].clientY;

				onMouseDownMouseX = clientX;
				onMouseDownMouseY = clientY;

				onMouseDownLon = lon;
				onMouseDownLat = lat;
				requestAnimationFrame(update);
			}

			function onPointerMove( event ) {
				if ( isUserInteracting === true ) {

					var clientX = event.clientX || event.touches[ 0 ].clientX;
					var clientY = event.clientY || event.touches[ 0 ].clientY;

					lon = ( onMouseDownMouseX - clientX ) * 0.1 + onMouseDownLon;
					lat = ( clientY - onMouseDownMouseY ) * 0.1 + onMouseDownLat;
					requestAnimationFrame(update);
				}
			}

			function onPointerUp() {
				isUserInteracting = false;
				requestAnimationFrame(update);
			}

			function onDocumentMouseWheel( event ) {
				var fov = camera.fov + event.deltaY * 0.05;

				camera.fov = THREE.Math.clamp( fov, 10, 75 );

				camera.updateProjectionMatrix();
				requestAnimationFrame(update);
			}

			function update() {

				lat = Math.max( - 85, Math.min( 85, lat ) );
				phi = THREE.Math.degToRad( 90 - lat );
				theta = THREE.Math.degToRad( lon );

				camera.target.x = 500 * Math.sin( phi ) * Math.cos( theta );
				camera.target.y = 500 * Math.cos( phi );
				camera.target.z = 500 * Math.sin( phi ) * Math.sin( theta );

				camera.lookAt( camera.target );

				renderer.render( scene, camera );
			}
		</script>
	</body>
</html>
