<?php
$images = scandir(__DIR__ . '/../gallery');
?>
<!DOCTYPE html>
<html>
<head>
    <meta name=viewport content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <title>360</title>
    <style>li {display:inline-block;}</style>
</head>
<body>
<?php if ($images) { ?>
    <ul>
    <?php foreach ($images as $img) {
        if ($img[0] == '.') continue;
        if ($img == 'thumbs') continue; ?>
        <li><a href="img.php?i=<?php echo urlencode($img); ?>"><img src="<?php echo "gallery/thumbs/" . htmlspecialchars($img); ?>" /></a></li>
    <?php } ?>
    </ul>
<?php } ?>
</body>
</html>
