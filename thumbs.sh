#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
FILE=
OUTPUTDIR=
SUFFIX=""

while getopts "o:" OPT; do
    case "$OPT" in
        o)
            OUTPUTDIR=$OPTARG
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Missing option argument for -$OPTARG" >&2
            exit 1
            ;;
    esac
done
shift $((OPTIND -1)) # skip to the remaining argument after options
if [ "" = "$1" ]; then
    echo "Empty file argument" >&2
    exit 1
fi

for FILE in "$@"
do
    FILE=`readlink -f "$FILE"`
    echo "Using file: $FILE"

    if [ ! -f "$FILE" ]; then
        echo "Specified file does not exist or is not a file" >&2
        continue
    fi
    echo "Processing $FILE"

    FILENAME="${FILE##*/}"
    FILEPATH="${FILE%/$FILENAME}"
    EXT="${FILENAME#*.}"
    FILENAMENOEXT="${FILENAME%.$EXT}"

    # echo $FILENAME
    # echo $FILEPATH
    # echo $EXT
    # echo $FILENAMENOEXT

    echo "parsed path: $FILEPATH / $FILENAMENOEXT . $EXT"
    if [ "" = "$OUTPUTDIR" ]; then
        OUTPUTDIR=$FILEPATH
        SUFFIX="-thumb"
    fi
    OUTPUTDIR=`readlink -f "$OUTPUTDIR"`
    echo "Using output dir $OUTPUTDIR"

    set -e

    mkdir -p "$OUTPUTDIR"

    # detect original image dimensions
    DIMENSIONS=`identify "$FILE" | cut -d" " -f3`
    ORIGWIDTH=`echo "$DIMENSIONS" | cut -d"x" -f1`
    ORIGHEIGHT=`echo "$DIMENSIONS" | cut -d"x" -f2`

    # calculate crop params
    # bash cannot do float divisions, only int, so we have to use awk
    WIDTH=$(awk "BEGIN {printf \"%.0f\",1600/(5514/${ORIGWIDTH})}") # 1600 for orig width 5514
    HEIGHT=$(awk "BEGIN {printf \"%.0f\",900/(2757/${ORIGHEIGHT})}") # 900 for orig width 2757
    LEFT=$(awk "BEGIN {printf \"%.0f\",(${ORIGWIDTH}-${WIDTH})/2}") # left = (image width - crop width) / 2
    TOP=$(awk "BEGIN {printf \"%.0f\",($ORIGHEIGHT-$HEIGHT)/2}") # top = (image height - crop height) / 2

    # make thumbnail
    echo "cropping ${WIDTH}x${HEIGHT}+${LEFT}+${TOP}"
    convert -crop "${WIDTH}x${HEIGHT}+${LEFT}+${TOP}" "$FILE" "$OUTPUTDIR/$FILENAMENOEXT$SUFFIX.jpg"
    convert -resize 300X169 "$OUTPUTDIR/$FILENAMENOEXT$SUFFIX.jpg" "$OUTPUTDIR/$FILENAMENOEXT$SUFFIX.jpg"

    set +e
    echo
    echo "####################################################################"
    echo
done
